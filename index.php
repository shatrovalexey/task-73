<?php
/**
* Разбор строки сообщения о платеже
*
* @param string $str - строка с сообщением о платеже
*
* @throws \Exception
* @return mixed - разобранное сообщение
*/
function decompose( $str ) {
	$result = [ ] ;

	if ( ! preg_match( '{(\b\d{4}\b)(?:[^р]|$)}ius' , $str , $matches ) ) {
		throw new \Exception( 'Пароль не найден' ) ;
	}
	$result[ 'password' ] = $matches[ 1 ] ;

	if ( ! preg_match( '{\b\d{11,20}\b}us' , $str , $matches ) ) {
		throw new \Exception( 'Номер кошелька не найден' ) ;
	}
	$result[ 'ym_id' ] = $matches[ 0 ] ;

	if ( ! preg_match( '{\b(\d+(?:[,\.]\d+)?)р\.}ius' , $str , $matches ) ) {
		throw new \Exception( 'Сумма не найдена' ) ;
	}
	$result[ 'amount' ] = $matches[ 0 ] ;

	return $result ;
}

if ( empty( $argv[ 1 ] ) ) {
	throw new \Exception( 'Не передана строка' ) ;
}

$result = decompose( $argv[ 1 ] ) ;

/*
$result = decompose( '
Пароль: 0947
Спишется 1240,21р.
Перевод на счет 41001100626683
' ) ;
*/

echo json_encode( $result ) ;